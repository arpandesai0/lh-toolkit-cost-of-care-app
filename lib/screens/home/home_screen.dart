import 'package:cost_of_care/bloc/location_bloc/location_bloc.dart';
import 'package:cost_of_care/bloc/location_bloc/user_location_events.dart';
import 'package:cost_of_care/bloc/nearby_hospital_bloc/nearby_hospital_bloc.dart';
import 'package:cost_of_care/bloc/nearby_hospital_bloc/nearby_hospital_event.dart';
import 'package:cost_of_care/screens/home/components/body.dart';
import 'package:cost_of_care/widgets/user_location.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../main.dart';
import 'components/app_bar_home.dart';

class Home extends StatefulWidget {
  Home(this.drawerKey);

  final GlobalKey<ScaffoldState> drawerKey;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int page = 2;

  /// page number one added when app opens.
  /// This is when user reaches end of page 1
  static const Color appBackgroundColor = Color(0xFFFFF7EC);
  final userLocationWidget = UserLocation(appBackgroundColor);
  ScrollController _scrollController = ScrollController();
  @override
  void initState() {
    super.initState();
    BlocProvider.of<LocationBloc>(context).add(FetchLocation());
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _scrollController.addListener(() {
        final maxScroll = _scrollController.position.maxScrollExtent;
        final currentScroll = _scrollController.position.pixels;
        if (maxScroll - currentScroll <= 100) {
          String state = box.get('state');
          context.read<NearbyHospitalBloc>().add(FetchHospitals(state, page++));
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      body: MediaQuery.removePadding(
        removeTop: true,
        context: context,
        child: CustomScrollView(
          controller: _scrollController,
          slivers: [
            SliverAppBar(
              backgroundColor: Colors.transparent,
              flexibleSpace: AppBarHome(widget.drawerKey),
              toolbarHeight: 90,
              floating: true,
              elevation: 0,
            ),
            Body(userLocationWidget),
          ],
        ),
      ),
    );
  }
}
