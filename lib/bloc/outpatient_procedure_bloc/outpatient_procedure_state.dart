part of 'outpatient_procedure_bloc.dart';

abstract class OutpatientProcedureState extends Equatable {
  const OutpatientProcedureState();
}

class OutpatientProcedureInitial extends OutpatientProcedureState {
  @override
  List<Object> get props => [];
}

class OutpatientProcedureLoadingState extends OutpatientProcedureState {
  @override
  List<Object> get props => [];
}

class OutpatientProcedureLoadedState extends OutpatientProcedureState {
  final List<OutpatientProcedure> outpatientProcedure;

  OutpatientProcedureLoadedState(this.outpatientProcedure);
  @override
  List<Object> get props => [outpatientProcedure];
}

class OutpatientProcedureErrorState extends OutpatientProcedureState {
  final String message;

  OutpatientProcedureErrorState(this.message);
  @override
  List<Object> get props => [message];
}
